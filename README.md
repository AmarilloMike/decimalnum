# decimalnum
'decimalnum' is a Structure and associated Methods employed in managing decimal numbers.

It is written in the Go Programming Language (a.k.a 'golang').

## Source Code Repository

The 'decimalnum' source code respository is located at:

https://bitbucket.org/AmarilloMike/decimalnum/src

## Dependencies
'decimalnum' is dependent on the following source code files:

### intary.go
'decimalnum' is dependent on the 'IntAry'structure and methods. The source code repository for 'IntAry' is:

https://bitbucket.org/AmarilloMike/intary/src

### nthroot.go
'decimalnum' is dependent on the 'NthRootOp' structure and methods. The source code respository for 'NthRootOp' is:

https://bitbucket.org/AmarilloMike/mathhlpr/src

### numstrdto.go
'decimalnum' is dependent on the 'NumStrDto' structure and methods. The source code respository for 'NumStrDto' is:

https://bitbucket.org/AmarilloMike/numstrutility/src